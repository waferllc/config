# README #

These can be used to install on new machines. If new modulefiles are created, they should be added.  

The init module is special. It defines ENVIRONMNT VARIABLES that are needed across all modulefiles. The main variable is the OPT_INSTALL_DIR which specifies where to find the installation of the modules (i.e. where did you install things?) This accomdates systems that have different installation areas - but you could be used for other things as well.  


Current modules:
anaconda/5.2.0 # This is outdated and may not work with the current installer env  
modelsim/2019.2  
osl/ultra96v2  
petalinux/2020.2  
petalinux/2018.3  
vivado/2020.2  
vivado/2018.3  

For Environment Module installation:  
sudo apt install -y tcl-dev environment-modules   
echo "source /etc/profile.d/modules.sh" >> ~/.bashrc  
sudo sed -i "s|^/|#/|g" /etc/environment-modules/modulespath  
echo "/opt/modulefiles" |  sudo tee -a /etc/environment-modules/modulespath  
sudo mkdir /opt/modulefiles  
sudo chown -R ${SSH_USER}:${SSH_USER} /opt/modulefiles  
sudo chown ${SSH_USER}:${SSH_USER} -R /usr/share/modules/modulefiles  
