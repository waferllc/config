#!/bin/bash  -eux

if [ -z $1 ]; then
    echo "No user specified"
    exit 0
fi

USER=$1
# SSH_KEY="${2:-""}"

# Create user (if not already present)
if ! id -u ${USER} >/dev/null 2>&1; then
    echo "==> Creating user ${USER}"
    if ! grep -q ${USER} /etc/group; then
        groupadd ${USER}
    fi
    useradd -s /bin/bash -m -g ${USER} -G sudo,dialout,nogroup,vboxsf ${USER}
    usermod -p $(openssl passwd -1 ${USER}) ${USER}
fi

# Set up sudo
echo "==> Giving sudo powers"
echo "${USER}       ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers.d/${USER}
chmod 440 /etc/sudoers.d/${USER}

# One might ask if this would be better suited as a non-privileged script...
# Adding SSH key
if [ ! -d /home/${USER}/.ssh ]; then
    echo "==> Adding .ssh folder"
    mkdir /home/${USER}/.ssh
    chown ${USER}:${USER} /home/${USER}/.ssh
    chmod 700 /home/${USER}/.ssh
fi

apt install keychain -y

apt install -y tcl-dev environment-modules 
sed -i "s|^/|#/|g" /etc/environment-modules/modulespath
echo "/opt/modulefiles" >> /etc/environment-modules/modulespath

mkdir /opt/modulefiles
chown -R ${USER}:${USER} /opt/modulefiles

# Check to see if SSH keys were copied in
if [ -f /home/vagrant/.ssh/my_id_rsa ]; then
    cp /home/vagrant/.ssh/my_id_rsa /home/${USER}/.ssh/id_rsa
    cp /home/vagrant/.ssh/my_id_rsa.pub /home/${USER}/.ssh/id_rsa.pub
    chown ${USER}:${USER} /home/${USER}/.ssh/id_rsa
    chown ${USER}:${USER} /home/${USER}/.ssh/id_rsa.pub
    chmod 600 /home/${USER}/.ssh/id_rsa
    chmod 644 /home/${USER}/.ssh/id_rsa.pub

    if [ ! -f /home/${USER}/.ssh/authorized_keys ]; then
        echo "==> Creating authorized_keys"
        touch /home/${USER}/.ssh/authorized_keys
        chown ${USER}:${USER} /home/${USER}/.ssh/authorized_keys
        chmod 600 /home/${USER}/.ssh/authorized_keys
    fi

    echo "==> Adding SSH key"
    cat /home/${USER}/.ssh/id_rsa.pub >> /home/${USER}/.ssh/authorized_keys

    ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts

    GIT_SSH_COMMAND="ssh -i /home/${USER}/.ssh/id_rsa" git clone git@bitbucket.org:waferllc/config.git /home/${USER}/config
    chown -R ${USER}:${USER} /home/${USER}/config

    cp /home/${USER}/.bashrc /home/${USER}/.bashrc.orig
    chown ${USER}:${USER} /home/${USER}/.bashrc.orig
    cp /home/${USER}/config/bash/.bashrc /home/${USER}/.bashrc
    cp /home/${USER}/config/bash/.inputrc /home/${USER}/.inputrc
    cp /home/${USER}/config/vim/.vimrc /home/${USER}/
    cp /home/${USER}/config/vim/.vimrc.plug /home/${USER}/
    cp -r /home/${USER}/config/vim/.vim /home/${USER}/
    chown -R ${USER}:${USER} /home/${USER}

    cp -r /home/${USER}/config/modulefiles/* /opt/modulefiles
else
    # Update .bashrc, .inputrc, modulefiles and other locally, or have them copied in Vagrant file provisioner?
fi





