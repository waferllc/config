# README #

The Vagrantfile is really user defined. This is just an example to start from that includes hopefully a good amount of options (many of which are commented out).   

This assume Packer is building the box files. However, the Vagrantfile could be used to run (slightly modified) provisioning scripts to accomplish the same thing.  The main different is it is strictly limited to Vagrant box files, and it is harder to set the disk size in Vagrant.   

Download https://www.vagrantup.com/downloads  


Useful Vagrant commands

vagrant box add --force --name \<box name> \<packer output box>  

vagrant up  
vagrant reload --provision  
vagrant suspend  
vagrant halt  
vagrant destroy  

vagrant box list  
vagrant box remove \<box name>  

