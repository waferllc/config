# README #

Packer template files for building VirtualBox machines with an optional post processor to create Vagrant box file.   

Packer can be used to create almost any virtual machine, but the files here are specific to Ubuntu guests in VirtualBox.  

There are several base files that use variables that can be modified with shorter var files. The shorter var files are easier to read and modify. For major changes, a new template file can be create.   

The JSON templates reference scripts that do the bulk of the configuration and installation. They are in the scripts folder. Some combination of them are run in different templates.  

Download: https://www.packer.io/downloads  

To build a machine:  

packer build --force -var-file=ubuntu2004.json ubuntu.json  

In this example ubuntu2004.json is the var file that modifies variables in the ubuntu.json template.  

NOTE: Other version can be specified in the iso_file, but...Ubuntu 20.04 changed the automated installer. So to support older versions you would need to modify the bootcmd and make a preseed file.   

To add an output box in Vagrant:   
vagrant box add --force --name \<vagrant box name> \<packer output>/ubuntu-20.04.box  
 
The Ubuntu 20.04 Subiquity installer uses information in http/user-data to drive the installer, as well as the boot_command in the JSON template. The user-data file is where the user and password are created. The password is encrypted.  

The base templates:  
ubuntu.json  
ubuntu-vivado.json   
ubuntu-petalinux.json  
 
In reality the user would create the var file, but as an example ubuntu-2004-desktop.json is provided.  

Customizable variables include:  
*headless* - Bool true|false if the VM starts headless  
*iso_url* - Path to ISO install image  
*iso_checksum* - ISO checksum   
*ssh_username* - Default username created. If you want to use a Vagrant box, its easiest to leave this as vagrant.  
*ssh_password* - Password to use for SSH - this is not how the user password *gets set* - that is in http/user-data.  
*cpus* - Number of CPU cores to configure in the VM  
*memory* - Megabytes of RAM to configure in the VM  
*disk_size* - Size of the disk image in Megabytes to configure in the VM  
*ssh_nat_port* - SSH NAT port, default is 2222  
*http_get* - 0|1 value indicating if an http server is providing files for download (instead of using the file provisioner). The server name of IP would get set here.   
*http_server* - The download server name or IP would get set here.  
*http_port* - This is the port to use for getting files.  
*vm_name* - Name of the VM output file  
*box_name* - Name of the box file created by the Vagrant post processor  
add_vagrant - 0|1 value to indicate whether to add a vagrant user and setup SSH keys   
*install_desktop* - 0|1 value to indicate if ubuntu-desktop is installed  
*install_update* - 0|1 value to indicate if apt update is run  
*install_upgrade* - 0|1 value to indicate if apt upgrade is run  
*install_additions* - 0|1 value to indicate if Virtualbox Additions are installed. These will already be installed if add_vagrant is set to 1.   
*install_vivado* - 0|1 value to indicated if Vivado is installed. This has a few requirements in order to work, see Installer Note.    
modified if you are installing a different version than 2020.2_1118_1232  
*install_petalinux* - 0|1 value to indicate if petalinux is installed. This has a few requirements in order to work, see Installer Note.    
*vivado_installer* - This is the name of the vivado installer file.  
*petalinux_installer* - This is the name of the petalinux installer file.  
*petalinux_ver* - This is the version number of petalinux that is being installed.  

NOTE: Not all combinations make sense, e.g. Vivado with no desktop.

**Installers**  
The installer binaries need to be copied into the VM. The installer file names are set as variables as well.  

There are two options:  
1. Use a file provisioner in the Packer JSON
2. Use an HTTP server and wget the file in the script.  

To use HTTP, make sure http_get is set to 1, and http_server and http_port are set correctly. If http_set is set to 0, the scripts will assume the installer files have been copied to the SSH_USER home directory.  

For Vivado you need the installer binary, the authentication file wi_authentication_token, and the installation configuration install_config.txt.  

To generate the authetication token file:   
  ./Xilinx_Unified_2020.2_1118_1232_Lin64.bin -- -b AuthTokenGen    
  It will be created in ~/.Xilinx/wi_authentication_key  
  Note: It is only value for 7 days.    

To generate the install config txt file:   
  ./Xilinx_Unified_2020.2_1118_1232_Lin64.bin -- -b ConfigGen  
  In most cases select (1) Vitis  
  Modify the necesary fields before copying to VM  

**HTTP Server**  
There are a couple options. A more permanent option is to install apache on a machine and setup a DocumentRoot directory with the files you need.  
Another options that is a little faster, but temporary, is to use the Python http.server module:    
  python -m http.server  

This command will setup a sever at http://<localip>:8000.  It will serve any files in the directory it is run in.  


