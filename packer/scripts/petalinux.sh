#!/bin/bash -eux

echo "==> Running petalinux.sh"
echo "==> INSTALL_PETALINUX=${INSTALL_PETALINUX}"
echo "==> HTTP_GET=${HTTP_GET}"
echo "==> HTTP_SERVER=${HTTP_SERVER}"
echo "==> HTTP_PORT=${HTTP_PORT}"
echo "==> PETALINUX_INSTALLER=${PETALINUX_INSTALLER}"
echo "==> PETALINUX_VER=${PETALINUX_VER}"
echo "==> SKIP_LICENSE=${SKIP_LICENSE}"

if [[ ! "$INSTALL_PETALINUX" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

SSH_USER=${SSH_USERNAME:-vagrant}

# This assumes the petalinux installer has already been copied through a file provisioner in Packer, 
# or is available from a # webserver.
# If they are copied in from Packer, the assumptions is they are all in /home/${SSH_USER}.

# Try to download
if [[ "$HTTP_GET" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  wget http://${HTTP_SERVER}:${HTTP_PORT}/${PETALINUX_INSTALLER}
fi

chmod u+x ${PETALINUX_INSTALLER}
./${PETALINUX_INSTALLER} --dir /opt/Xilinx/petalinux/${PETALINUX_VER}

exit 0