#!/bin/bash -eux

echo "==> Running vivado.sh"
echo "==> INSTALL_VIVADO=${INSTALL_VIVADO}"
echo "==> HTTP_GET=${HTTP_GET}"
echo "==> HTTP_SERVER=${HTTP_SERVER}"
echo "==> HTTP_PORT=${HTTP_PORT}"
echo "==> VIVADO_INSTALLER=${VIVADO_INSTALLER}"
echo "==> PETALINUX_VER=${INSTALL_DIR}"

if [[ ! "$INSTALL_VIVADO" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

SSH_USER=${SSH_USERNAME:-vagrant}

# This assumes files (Xilinx install bin, wi_authentication_key, install_config.txt) have
# already been copied through a file provisioner in Packer, or are available from a 
# webserver.
# If they are copied in from Packer, the assumptions is they are all in /home/${SSH_USER}.
# 
#  To generate the authetication token file:
#      ./Xilinx_Unified_2020.2_1118_1232_Lin64.bin -- -b AuthTokenGen
#      It will be in ~/.Xilinx/wi_authentication_key
#  To generate the install_config.txt file:
#      ./Xilinx_Unified_2020.2_1118_1232_Lin64.bin -- -b ConfigGen
#       In most cases select (1) Vitis
#       It will be in ~/.Xilinx/install_config.txt
#       Modify the necesary fields.

# Try to download
if [[ "$HTTP_GET" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  wget http://${HTTP_SERVER}:${HTTP_PORT}/${VIVADO_INSTALLER}
  wget http://${HTTP_SERVER}:${HTTP_PORT}/install_config.txt
  wget http://${HTTP_SERVER}:${HTTP_PORT}/wi_authentication_key
fi

chmod u+x /home/${SSH_USER}/Xilinx_Unified_2020.2_1118_1232_Lin64.bin
mkdir /home/${SSH_USER}/.Xilinx
cp wi_authentication_key /home/${SSH_USER}/.Xilinx
cp install_config.txt /home/${SSH_USER}/.Xilinx

./Xilinx_Unified_2020.2_1118_1232_Lin64.bin -- -b Install -c /home/${SSH_USER}/install_config.txt -a XilinxEULA,3rdPartyEULA,WebTalkTerms

exit 0