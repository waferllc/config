#!/bin/bash

echo "==> Running user.sh"

SSH_USER=${SSH_USERNAME:-vagrant}

apt install -y libncurses5
apt install -y vim vim-gtk3
apt install -y openssh-server net-tools
apt install -y git
apt install -y gnome-tweaks chrome-gnome-shell
apt install -y python3-serial minicom picocom qemu-user-static

# git clone .vimrc and .bashrc?

# VS Code
apt install software-properties-common apt-transport-https
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
apt update -y
apt install code -yes

# VirtualHere client
wget https://www.virtualhere.com/sites/default/files/usbclient/scripts/virtualhereclient.service
wget https://www.virtualhere.com/sites/default/files/usbclient/vhclientx86_64
chmod +x ./vhclientx86_64
mv ./vhclientx86_64 /usr/sbin
mv virtualhereclient.service /etc/systemd/system/virtualhereclient.service
systemctl enable virtualhereclient.service
systemctl start virtualhereclient.service

# Environment Modules
apt install -y tcl-dev environment-modules 
echo "source /etc/profile.d/modules.sh" >> ~/.bashrc
sed -i "s|^/|#/|g" /etc/environment-modules/modulespath
echo "/opt/modulefiles" >> /etc/environment-modules/modulespath
mkdir /opt/modulefiles
chown -R ${SSH_USER}:${SSH_USER} /opt/modulefiles
chown ${SSH_USER}:${SSH_USER} -R /usr/share/modules/modulefiles

# git clone environment modules

# chown /opt to ${SSH_USER}?

exit 0