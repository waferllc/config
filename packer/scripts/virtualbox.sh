#!/bin/bash -eux

# From https://github.com/boxcutter/ubuntu/tree/master/script

echo "==> Running virtualbox.sh"
echo "==> INSTALL_ADDITIONS=${INSTALL_ADDITIONS}"
echo "==> PACKER_BUILDER_TYPE=${PACKER_BUILDER_TYPE}"
echo "==> VBOX_VERSION=${VBOX_VERSION}"

if [[ ! "$INSTALL_ADDITIONS" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

SSH_USER=${SSH_USERNAME:-vagrant}

if [[ $PACKER_BUILDER_TYPE =~ virtualbox ]]; then
    apt update -y
    apt install gcc make perl -y
    
    echo "==> Installing VirtualBox guest additions"
    apt install -y linux-headers-$(uname -r) build-essential perl
    apt install -y dkms

    VBOX_VERSION=$(cat /home/${SSH_USER}/.vbox_version)
    mount -o loop /home/${SSH_USER}/VBoxGuestAdditions_$VBOX_VERSION.iso /mnt
    sh /mnt/VBoxLinuxAdditions.run
    umount /mnt
    rm /home/${SSH_USER}/VBoxGuestAdditions_$VBOX_VERSION.iso
    rm /home/${SSH_USER}/.vbox_version

fi

exit 0