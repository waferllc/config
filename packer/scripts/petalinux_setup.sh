#!/bin/bash -eux

echo "==> Running petalinux_setup.sh"
echo "==> INSTALL_PETALINUX=${INSTALL_PETALINUX}"
echo "==> PETALINUX_VER=${PETALINUX_VER}"
echo "==> PETALINUX_VER=${INSTALL_DIR}"

if [[ ! "$INSTALL_PETALINUX" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

SSH_USER=${SSH_USERNAME:-vagrant}

apt install expect gawk xterm autoconf libtool texinfo gcc-multilib libncurses-dev  tmux gparted qemu-user-static -y

apt install -y tftpd-hpa
sed -i "s|/tftpboot|/tftp|g" /etc/default/tftpd-hpa
sed -i "s|--secure|--secure --create|g" /etc/default/tftpd-hpa
mkdir /tftp
chown -R ${SSH_USER}:${SSH_USER} /tftp
chmod 777 /tftp
systemctl restart tftpd-hpa


mkdir -p ${INSTALL_DIR}/Xilinx/petalinux/${PETALINUX_VER}
chown -R ${SSH_USER}:${SSH_USER} /opt/Xilinx/petalinux

exit 0