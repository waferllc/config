#!/bin/bash

# From https://github.com/boxcutter/ubuntu/tree/master/script

echo "==> Running destop.sh"
echo "==> INSTALL_DESKTOP=${INSTALL_DESKTOP}"

if [[ ! "$INSTALL_DESKTOP" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

SSH_USER=${SSH_USERNAME:-vagrant}

# echo "==> Checking version of Ubuntu"
# . /etc/lsb-release

# cat /etc/X11/default-display-manager
# echo "/usr/sbin/gdm3" > /etc/X11/default-display-manager
# DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true dpkg-reconfigure gdm3
# echo set shared/default-x-display-manager gdm3 | debconf-communicate

# echo "/usr/sbin/lightdm" > /etc/X11/default-display-manager
# DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true dpkg-reconfigure lightdm
# echo set shared/default-x-display-manager lightdm | debconf-communicate

echo "==> Installing ubuntu-desktop"
apt-get install -y ubuntu-desktop

# echo "==> Installing XFCE"
# apt install xfce4 -y
# apt install xubuntu-desktop -y

echo "==> Installing XRDP"
apt install -y xrdp
adduser xrdp ssl-cert  
systemctl restart xrdp

# echo "==> Installing x2go"
# add-apt-repository ppa:x2go/stable
# apt-get update
# apt-get install -y x2goserver x2goserver-xsession
# apt-get install -y x2golxdebindings

# Disable crash reporting
sed -i "s|enabled=1|enabled=0|g" /etc/default/apport

# Get rid of welcome screen
echo "InitialSetupEnable=false" >> /etc/gdm3/custom.conf

sudo sed -i 's/allowed_users=.*$/allowed_users=anybody/' /etc/X11/Xwrapper.config

# USERNAME=${SSH_USER}
# LIGHTDM_CONFIG=/etc/lightdm/lightdm.conf
# GDM_CUSTOM_CONFIG=/etc/gdm3/custom.conf

# if [ -f $GDM_CUSTOM_CONFIG ]; then
#     mkdir -p $(dirname ${GDM_CUSTOM_CONFIG})
#     > $GDM_CUSTOM_CONFIG
#     echo "[daemon]" >> $GDM_CUSTOM_CONFIG
#     echo "# Enabling automatic login" >> $GDM_CUSTOM_CONFIG
#     echo "AutomaticLoginEnable = true" >> $GDM_CUSTOM_CONFIG
#     echo "AutomaticLogin = ${USERNAME}" >> $GDM_CUSTOM_CONFIG
# fi

# if [ -f $LIGHTDM_CONFIG ]; then
#     echo "==> Configuring lightdm autologin"
#     echo "[SeatDefaults]" >> $LIGHTDM_CONFIG
#     echo "autologin-user=${USERNAME}" >> $LIGHTDM_CONFIG
#     echo "autologin-user-timeout=0" >> $LIGHTDM_CONFIG
# fi

# if [ -d /etc/xdg/autostart/ ]; then
#     echo "==> Disabling screen blanking"
#     NODPMS_CONFIG=/etc/xdg/autostart/nodpms.desktop
#     echo "[Desktop Entry]" >> $NODPMS_CONFIG
#     echo "Type=Application" >> $NODPMS_CONFIG
#     echo "Exec=xset -dpms s off s noblank s 0 0 s noexpose" >> $NODPMS_CONFIG
#     echo "Hidden=false" >> $NODPMS_CONFIG
#     echo "NoDisplay=false" >> $NODPMS_CONFIG
#     echo "X-GNOME-Autostart-enabled=true" >> $NODPMS_CONFIG
#     echo "Name[en_US]=nodpms" >> $NODPMS_CONFIG
#     echo "Name=nodpms" >> $NODPMS_CONFIG
#     echo "Comment[en_US]=" >> $NODPMS_CONFIG
#     echo "Comment=" >> $NODPMS_CONFIG
# fi

exit 0