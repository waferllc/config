#!/bin/bash -eux

echo "==> Running vagrant.sh"
echo "==> ADD_VAGRANT=${ADD_VAGRANT}"
echo "==> VAGRANT_INSECURE_KEY=${VAGRANT_INSECURE_KEY}"

if [[ ! "$ADD_VAGRANT" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

SSH_USER=${SSH_USERNAME:-vagrant}

# By default vagrant will try insecure key if ssh_password is not set
# in the Vagrantfile. It will then copy a secure public key, but you need
# this one to give you access the first time.
# NOTE: Tried appending the secure key that it uses, and that doesnt seem to work?
# The insecure key is at C:\Users\<usernam>\.vagrant.d\insecure_private_key
# To get public key: ssh-keygen -y -f insecure_private_key
# or
# wget --no-check-certificate https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub' -O /home/vagrant/.ssh/authorized_keys
# VAGRANT_INSECURE_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key"


# Create Vagrant user (if not already present)
if ! id -u vagrant >/dev/null 2>&1; then
    echo "==> Creating vagrant user"
    groupadd vagrant
    useradd -s /bin/bash -m -G sudo vagrant
    usermod -p $(openssl passwd -1 vagrant) vagrant
fi

# Set up sudo
echo "==> Giving vagran sudo powers"
echo "vagrant       ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers.d/vagrant
chmod 440 /etc/sudoers.d/vagrant

# Create ssh keys
if [ ! -d /home/vagrant/.ssh ]; then
    echo "==> Adding vagrant .ssh folder"
    mkdir /home/vagrant/.ssh
    chmod 700 /home/vagrant/.ssh
fi
if [ ! -f /home/vagrant/.ssh/authorized_keys ]; then
    echo "==> Creating vagrant authorized_keys"
    touch /home/vagrant/.ssh/authorized_keys
    chmod 600 /home/vagrant/.ssh/authorized_keys
fi

echo "==> Adding Vagrant insecure SSH key"
echo "${VAGRANT_INSECURE_KEY}" >> /home/vagrant/.ssh/authorized_keys

chown -R vagrant:vagrant /home/vagrant/.ssh

# Fix stdin not being a tty
# if grep -q -E "^mesg n$" /root/.profile && sed -i "s/^mesg n$/tty -s \\&\\& mesg n/g" /root/.profile; then
#     echo "==> Fixed stdin not being a tty."
# fi

# Fix ssh 
sed -i "s/#UseDNS no/UseDNS no/g" /etc/ssh/sshd_config
sed -i "s/#GSSAPIAuthentication no/GSSAPIAuthentication no/g" /etc/ssh/sshd_config 

if [[ $PACKER_BUILDER_TYPE =~ virtualbox ]]; then
    apt update -y
    apt install gcc make perl -y

    echo "==> Installing VirtualBox guest additions"
    apt install -y linux-headers-$(uname -r) build-essential
    apt install -y dkms

    VBOX_VERSION=$(cat /home/${SSH_USER}/.vbox_version)
    mount -o loop /home/${SSH_USER}/VBoxGuestAdditions_$VBOX_VERSION.iso /mnt
    sh /mnt/VBoxLinuxAdditions.run
    umount /mnt
    rm /home/${SSH_USER}/VBoxGuestAdditions_$VBOX_VERSION.iso
    rm /home/${SSH_USER}/.vbox_version

fi
exit 0