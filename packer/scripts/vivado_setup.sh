#!/bin/bash -eux

echo "==> Running vivado_setup.sh"
echo "==> INSTALL_VIVADO=${INSTALL_VIVADO}"

if [[ ! "$INSTALL_VIVADO" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

SSH_USER=${SSH_USERNAME:-vagrant}

mkdir /opt/Xilinx
chown -R ${SSH_USER}:${SSH_USER} /opt/Xilinx

apt install libtinfo5 -y

exit 0