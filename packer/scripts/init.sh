#!/bin/bash -eux

echo "==> Running init.sh"
echo "==> INSTALL_UPDATE=${INSTALL_UPDATE}"
echo "==> INSTALL_UPGRADE=${INSTALL_UPGRADE}"

if [[ ! "$INSTALL_UPDATE" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
  exit 0
fi

# TODO: ADd check for all required ENV VARS?

SSH_USER=${SSH_USERNAME:-vagrant}

echo "==> Updating..."
apt-get update -y 

apt install gcc make perl -y

apt install nfs-kernel-server nfs-common openssh-server -y

apt install curl wget git screen tmux vim expect -y

if [[ $INSTALL_UPGRADE  =~ true || $INSTALL_UPGRADE =~ 1 || $INSTALL_UPGRADE =~ yes ]]; then
  echo "==> Upgrading..."
    apt upgrade -y
fi

exit 0